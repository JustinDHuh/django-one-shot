from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    list = TodoList.objects.all()
    context = {"todo_list": list}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {"todo_object": todo_detail}
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_list = form.save()
            print(new_list)
            return redirect("todo_list_detail", new_list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todolist = get_object_or_404(TodoList, id=id)

    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=todolist)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    TodoList_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        TodoList_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    todo_item_update = get_object_or_404(TodoItem, id=id)

    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item_update)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=todo_item_update)

    context = {
        "form": form,
    }
    return render(request, "todos/update_item.html", context)
